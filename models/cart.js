const { DataTypes } = require('sequelize')
const sequelize = require('../config')
const Product = require('../models/product')
const User = require('../models/user')
const Store = require('../models/store')

const Cart = sequelize.define('Cart', {
    quantity: {
        type: DataTypes.INTEGER
    },
    totalPrice: {
        type: DataTypes.INTEGER
    }
},{
    sequelize,
    tableName: 'carts',
    modelName: 'Cart'
})

Product.hasMany(Cart)
Cart.belongsTo(Product)

User.hasMany(Cart)
Cart.belongsTo(User)

Store.hasOne(Cart)
Cart.belongsTo(Store)

module.exports = Cart

