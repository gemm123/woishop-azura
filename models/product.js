const { DataTypes } = require('sequelize');
const sequelize = require('../config')

const Product = sequelize.define('Product', {
    name: {
        type: DataTypes.STRING
    },
    quantity: {
        type: DataTypes.INTEGER
    },
    price: {
        type: DataTypes.INTEGER
    },
    image: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    tableName: "products",
    modelName: "Product"
})

module.exports = Product