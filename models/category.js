const { DataTypes } = require('sequelize')
const sequelize = require('../config')

const Category = sequelize.define('Category', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'categories',
    modelName: 'Category'
})

module.exports = Category