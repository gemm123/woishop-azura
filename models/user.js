const { DataTypes } = require('sequelize')
const sequelize = require('../config')

const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING
    },
    image: {
        type: DataTypes.STRING
    },
    city: {
        type: DataTypes.STRING
    },
    kecamatan: {
        type: DataTypes.STRING
    },
    posCode: {
        type: DataTypes.INTEGER
    },
    address: {
        type: DataTypes.STRING
    },
    otp: {
        type: DataTypes.INTEGER
    },
    code: {
        type: DataTypes.STRING
    },
    codeReferral: {
        type: DataTypes.STRING,
    }
}, {
    sequelize,
    tableName: "users",
    modelName: "User"
})

module.exports = User