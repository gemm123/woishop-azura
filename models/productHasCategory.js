const { DataTypes } = require('sequelize')
const sequelize = require('../config')
const Product = require('./product')
const Category = require('./category')

const ProductHasCategory = sequelize.define('ProductHasCategory', {}, {
    sequelize,
    tableName: 'productHasCategory',
    modelName: 'ProductHasCategory'
})

Product.belongsToMany(Category, { through: ProductHasCategory, foreignKey: 'productId' })
Category.belongsToMany(Product, { through: ProductHasCategory, foreignKey: 'categoryId' })

module.exports = ProductHasCategory