const { DataTypes } = require('sequelize')
const sequelize = require('../config')
const Product = require('./product')

const Store = sequelize.define('Store', {
    name: {
        type: DataTypes.STRING
    },
    address: {
        type: DataTypes.STRING
    },
    rating: {
        type: DataTypes.INTEGER
    },
    image: {
        type: DataTypes.STRING
    },
    shippingMethod: {
        type: DataTypes.STRING
    }
}, {
    sequelize,
    tableName: 'stores',
    modelName: 'Store'
})

Store.hasMany(Product)
Product.belongsTo(Store)

module.exports = Store