const express = require('express')
const app = express()
const port = 3000

const categories = require('./routes/category')
const Category = require('./models/category')

const products = require('./routes/product')
const Product = require('./models/product')

const ProductHasCategory = require('./models/productHasCategory')

const auth = require('./routes/auth')
const User = require('./models/user')

const carts = require('./routes/cart')
const Cart = require('./models/cart')

const Store = require('./models/store')

app.use(express.json())

app.use('/api/v1/categories', categories)
app.use('/api/v1/products', products)
app.use('/api/v1/auth', auth)
app.use('/api/v1/carts', carts)

app.listen(port, async () => {
    console.log(`Server running at port: ${port}`)
    await Category.sync({ alter: true })
    await Product.sync({ alter: true })
    await ProductHasCategory.sync({ alter: true })
    await User.sync({ alter: true })
    await Cart.sync({ alter: true })
    await Store.sync({ alter: true })
})