const categoryRepository = require('../repository/category')

exports.getAllCategory = async (req, res) => {
    try {
        const categories = await categoryRepository.getAllCategory()
        
        res.status(200).json({ message: "success", data: categories })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

// exports.postCategory = async (req, res) => {
//     const { name } = req.body
    
//     try {
//         const category = await Category.create({ name: name })

//         res.status(200).json({ message: "success", data: category })
//     } catch (error) {
//         res.status(400).json({ message: `failed ${error.message}` })
//     }
// }