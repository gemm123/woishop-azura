const Cart = require('../models/cart')
const Product = require('../models/product')
const User = require('../models/user')
const cartRepository = require('../repository/cart')
const productRepository = require('../repository/product')

exports.getAllCart = async(req, res) => {
    try {
        const carts = await cartRepository.getAllUserCart(req.user.id)

        res.status(200).json({ message: "success", data: carts })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.getCartByID = async(req, res) => {
    const { id } = req.params

    try {
        const cart = await Cart.findOne({ 
            where: { id: id },
            include: [{
                model: Product
            },{
                model: User
            }]    
        })

        res.status(200).json({ message: "success", data: cart })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.postCart = async(req, res) => {
    const { idProduct } = req.body
    const quantity = parseInt(req.body.quantity)
    const totalPrice = parseInt(req.body.totalPrice)

    try {
        const product = await productRepository.getProductById(idProduct)
        if (!product) {
            res.status(400).json({ message: "failed product not found" })
            return
        }
        
        const cart = await cartRepository.createCart(quantity, totalPrice, idProduct, req.user.id, product.StoreId)

        res.status(200).json({ message: "success", data: cart })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })    
    }
}

exports.putCart = async(req, res) => {
    const { id } =  req.params
    const { idProduct } = req.body
    const quantity = parseInt(req.body.quantity)
    const totalPrice = parseInt(req.body.totalPrice)

    try {
        const product = await productRepository.getProductById(idProduct)
        if (!product) {
            res.status(400).json({ message: "failed product not found" })
            return
        }

        const updatedCart = await cartRepository.updateCart(id, quantity, totalPrice, idProduct)

        res.status(200).json({ message: "success", data: updatedCart})
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.deleteCart = async(req, res) => {
    const { id } = req.params

    try {
        await cartRepository.deleteCart(id)

        res.status(200).json({ message: "success delete cart"})
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })      
    }
}