const productRepository = require('../repository/product')

exports.getAllProduct = async (req, res) => {
    const categoryName = req.query.category

    if (categoryName) {
        if(typeof categoryName === "string") {
            try {
                const products = await productRepository.getAllProductByCategory(categoryName)
    
                res.status(200).json({ message: "success", data: products })
                return
            } catch (error) {
                res.status(400).json({ message: `failed ${error.message}` })
                return
            }
        } else {
            try {
                const products = await productRepository.getAllProductByArrayCategory(categoryName)

                res.status(200).json({ message: "success", data: products })
                return
            } catch (error) {
                res.status(400).json({ message: `failed ${error.message}` })
                return
            }
        }
    }

    try {
        const products = await productRepository.getAllProduct()
        
        res.status(200).json({ message: "success", data: products })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.getProductByID = async (req, res) => {
    const { id } = req.params
    
    try {
        const product = await productRepository.getProductById(id)

        res.status(200).json({ message: "success", data: product })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

// exports.postProduct = async (req, res) => {
//     const { name, quantity, price, image, category } = req.body

//     try {
//         const cate = await Category.findOne({ where: { name: category } })
//         if (cate === null) {
//             res.status(400).json({ message: "failed category not found" })
//         }

//         // const product = await Product.create({ name, quantity, price, image })
//         // await ProductCategory.create({ ProductId: product.id, CategoryId: category.id})

//         // res.status(200).json({ message: 'success', product: product, category: category.name })
//     } catch (error) {
//         res.status(400).json({ message: `faield ${error.message}` })
//     }
// }