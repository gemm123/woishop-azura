const { generateCode } = require('../helper')
const jwt = require('jsonwebtoken')
const axios = require('axios').default
const userRepository = require('../repository/user')
require('dotenv').config()

exports.register = async (req, res) => {
    const name = req.body.name
    const phone = req.body.phone
    const codeReferral = req.body.code_referral

    const otp = Math.floor(1000 + Math.random() * 9000);
    const code = generateCode(10)

    try {
        const user = await userRepository.getUser(phone)

        if (user !== null) {
            res.status(400).json({ message: "failed phone number has been used" })
            return
        }

        if (codeReferral) {
            const checkReferral = await userRepository.checkReferral(codeReferral)
            if (checkReferral) {
                res.status(400).json({ message: "failed code referral not found" })
                return
            }
        }
    
        const newUser = await userRepository.createUser(name, phone, codeReferral, otp, code)

        const config = {
            headers: { 
                Authorization: "Basic dXNtYW5ydWJpYW50b3JvcW9kcnFvZHJiZWV3b293YToyNjM3NmVkeXV3OWUwcmkzNDl1ZA=="
            }
        }
        const data = {
            phone_no: newUser.phone,
            key: "db63f52c1a00d33cf143524083dd3ffd025d672e255cc688",
            message: `Kode OTP anda adalah ${otp}`
        }
        axios
            .post('http://45.77.34.32:8000/demo/send_message', data, config)
            .then((res) => {
                return
            })
            .catch((err) => {
                console.log(err)
            })

        res.status(200).json({ message: "register success", data: newUser })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.login = async (req, res) => {
    const phone = req.body.phone
    
    try {
        const user = await userRepository.getUser(phone)
        if (user === null) {
            res.status(400).json({ message: "failed phone number not registered" })
            return
        }

        const newotp = Math.floor(1000 + Math.random() * 9000);

        await userRepository.updateOTP(user, newotp)

        const config = {
            headers: { 
                Authorization: "Basic dXNtYW5ydWJpYW50b3JvcW9kcnFvZHJiZWV3b293YToyNjM3NmVkeXV3OWUwcmkzNDl1ZA=="
            }
        }
        const data = {
            phone_no: user.phone,
            key: "db63f52c1a00d33cf143524083dd3ffd025d672e255cc688",
            message: `Kode OTP anda adalah ${newotp}`
        }
        axios
            .post('http://45.77.34.32:8000/demo/send_message', data, config)
            .then((res) => {
                return
            })
            .catch((err) => {
                console.log(err)
            })
        
        const token = jwt.sign({
            data: user
        }, `${process.env.SECRET_KEY}`, { expiresIn: '7d' })

        res.status(200).json({ message: "success",data: user, token: token })
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })
    }
}

exports.verificationOTP = async (req, res) => {
    const phone = req.body.phone
    const otp = parseInt(req.body.otp)

    try {
        const user = await userRepository.getUser(phone)
        if (user === null) {
            res.status(400).json({ message: "failed phone number not registered" })
            return
        }

        if (user.otp !== otp) {
            res.status(400).json({ message: "failed otp does not match" })
            return
        }

        const token = jwt.sign({
            data: user
        }, `${process.env.SECRET_KEY}`, { expiresIn: '7d' })

        res.status(200).json({ message: "success verification otp match", token: token})
    } catch (error) {
        res.status(400).json({ message: `failed ${error.message}` })  
    }
}