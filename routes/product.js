const express = require('express')
const router = express.Router()
const productController = require('../controllers/product')
const middleware = require('../middleware')

router.get('/', middleware.checkAuthorization, productController.getAllProduct)
router.get('/:id', middleware.checkAuthorization, productController.getProductByID)
// router.post('/create', productController.postProduct)

module.exports = router