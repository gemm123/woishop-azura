const express = require('express')
const router = express.Router()
const cartController = require('../controllers/cart')
const middleware = require('../middleware')

router.get('/', middleware.checkAuthorization, cartController.getAllCart)
router.get('/:id', middleware.checkAuthorization, cartController.getCartByID)
router.post('/create', middleware.checkAuthorization, cartController.postCart)
router.put('/:id', middleware.checkAuthorization, cartController.putCart)
router.delete('/:id', middleware.checkAuthorization, cartController.deleteCart)

module.exports = router