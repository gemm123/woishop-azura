const Category = require('../models/category')

exports.getAllCategory = async () => {
    const categories = await Category.findAll()
    
    return categories
}

exports.getCategoryByName = async (name) => {
    const categories = await Category.findOne({ 
        where: { 
            name 
        } 
    })

    return categories
}