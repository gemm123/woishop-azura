const User = require('../models/user')

exports.getUser = async(phone) => {
    const user = await User.findOne({
        where: {
            phone: phone
        }
    })

    if (user) {
        return user
    } else {
        return null
    }
}

exports.checkReferral = async (codeReferral) => {
    const referral = await User.findOne({
        where: {
            code: codeReferral
        }
    })

    if (referral) {
        return true
    } else {
        return false
    }
}

exports.createUser = async (name, phone, codeReferral, otp, code) => {
    const user = await User.create({ 
        name,
        phone,
        codeReferral,
        otp,
        code
    })

    return user
}

exports.updateOTP = async (user, newOTP) => {
    user.otp = newOTP
    await user.save()
}