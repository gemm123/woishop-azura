const Cart = require('../models/cart')
const Product = require('../models/product')
const User = require('../models/user')

exports.getAllUserCart = (id) => {
    const allUserCart = Cart.findAll({ 
        where: {
            UserId: id
        },
        include: [{
            model: Product
        }, {
            model: User
        }]
    })

    return allUserCart
}

exports.createCart = async (quantity, totalPrice, ProductId, UserId, StoreId) => {
    const cart = await Cart.create({
        quantity,
        totalPrice,
        ProductId,
        UserId,
        StoreId
    })

    return cart
}

exports.updateCart = async (id, quantity, totalPrice, ProductId) => {
    const cart = await Cart.findOne({
        where: {
            id: id
        }
    })

    cart.quantity = quantity
    cart.totalPrice = totalPrice
    cart.ProductId = ProductId

    cart.save()

    return cart
}

exports.deleteCart = (id) => {
    Cart.destroy({
        where: {
            id: id
        }
    })

    return
}