const Product = require('../models/product')
const Category = require('../models/category')
const { Op } = require("sequelize");

exports.getAllProduct = async () => {
    const products = await Product.findAll()

    return products
}

exports.getAllProductByCategory = async (categoryName) => {
    const products = await Product.findAll({
        include: {
            model: Category,
            where: {
                name: categoryName
            }
        }
    })

    return products
}

exports.getAllProductByArrayCategory = async (categoryName) => {
    const products = await Product.findAll({
        include: {
            model: Category,
            where: {
                name: { [Op.in]: categoryName }
            }
        }
    })

    return products
}

exports.getProductById = async (id) => {
    const product = await Product.findOne({ 
        where: { 
            id: id 
        } 
    })

    return product
}